package com.example;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.mongodb.ParkingPlaceRepository;
import com.mongodb.dao.CommentMongo;
import com.mongodb.dao.ParkingPlaceMongo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebAppConfiguration
public class TestMongo {

  @Autowired
  private ParkingPlaceRepository repository;

  @Before
  public void init() {
    List<CommentMongo> comments = new ArrayList<>();
    comments.add(new CommentMongo(1, "User1", "12 First", 5));
    comments.add(new CommentMongo(2, "User2", "Se cond", 4));
    comments.add(new CommentMongo(3, "User3", "12top10", 3));
    repository.save(new ParkingPlaceMongo(1, comments, "dsfsd"));
    repository.save(new ParkingPlaceMongo(2, null, "qqq"));
  }

  @Test
  public void testMongo() {
    ParkingPlaceMongo result3 = repository.getByCommentsText("12");
    System.out.println(result3);
    assertNotNull(result3);
  }

  @Test
  public void likeSearch() {
    List<ParkingPlaceMongo> result3 = repository.findByCommentsTextLike("12");
    System.out.println(result3.size());
    assertNotNull(result3.size());
  }

  @Test
  public void indexSearch() {
    List<ParkingPlaceMongo> result3 = repository.getByCommentsTextIndex("12");
    System.out.println(result3.size());
    assertNotNull(result3.size());
  }
  
}
