package com.example;


import com.dao.DataBaseDAO;

public class ThreadTask implements Runnable {

  private Integer from;
  private Integer to;
  
  public ThreadTask(Integer from, Integer to, DataBaseDAO dataBaseDAO) {
    super();
    this.from = from;
    this.to = to;
    this.dataBaseDAO = dataBaseDAO;
  }
  private DataBaseDAO dataBaseDAO;
  
  @Override
  public void run() {
    
    dataBaseDAO.updateDealEmployee(from,to);
  }

}
