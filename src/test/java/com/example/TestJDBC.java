package com.example;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.dao.DataBaseDAO;
import com.data.Client;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebAppConfiguration

public class TestJDBC {
  @Autowired
  private DataBaseDAO dataBaseDAO;

  @Autowired
  private TaskExecutor taskExecutor;


  @Test
  public void testJDBC() {

    Client client = dataBaseDAO.getClient(1);
    System.out.println(client.name);

  }

  @Test
  public void testAd() {

    for (int i = 0; i < 10; i++) {
      taskExecutor.execute(new ThreadTask(17, 15,dataBaseDAO));
    }
    try {
      Thread.sleep(500000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("BA-DUM-TSSSS");
  }

}
