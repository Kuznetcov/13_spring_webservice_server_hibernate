package com.dao.data;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "\"Roles\"")
@JsonIgnoreProperties({"users"})
public class RolesDAO implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = -1848919536800974833L;
  @Id
  @Column(name = "id",unique = true, nullable = false)
  public Integer id;
  @Column(name = "name")
  public String name;
  
  @JsonIgnore
  @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
  public Set<UserDAO> users = new HashSet<UserDAO>(0);

  public RolesDAO(Integer id, String name, Set<UserDAO> users) {
    super();
    this.id = id;
    this.name = name;
    this.users = users;
  }

  public RolesDAO(Integer id, String name) {
    super();
    this.id = id;
    this.name = name;
  }

  public RolesDAO(String name) {
    super();
    this.name = name;
  }

  public RolesDAO() {
    super();
  }

  public synchronized Integer getId() {
    return id;
  }

  public synchronized void setId(Integer id) {
    this.id = id;
  }

  public synchronized String getName() {
    return name;
  }

  public synchronized void setName(String name) {
    this.name = name;
  }

  @JsonIgnore
  public synchronized Set<UserDAO> getUsers() {
    return users;
  }

  @JsonIgnore
  public synchronized void setUsers(Set<UserDAO> users) {
    this.users = users;
  }

  @Override
  public String toString() {
    return "RolesDAO [id=" + id + ", name=" + name + "]";
  }
  
}
