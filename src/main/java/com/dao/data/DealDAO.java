package com.dao.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Deals\"")
public class DealDAO implements Serializable {

  
  /**
   * 
   */
  private static final long serialVersionUID = -4949333657003574338L;
  
  @Id 
  @Column(name = "id")
  public Integer dealID;
  
  @Column(name = "date")
  public Date date;
  
  @Column(name = "client")
  public Integer clientID;
  
  @Column(name = "employee")
  public Integer employeeID;
  
  @Column(name = "place")
  public Integer parkingPlaceID;
  
  @Column(name = "car")
  public Integer carNumber;
  
  
  public synchronized Integer getDealID() {
    return dealID;
  }
  public synchronized void setDealID(Integer dealID) {
    this.dealID = dealID;
  }
  public synchronized Date getDate() {
    return date;
  }
  public synchronized void setDate(Date date2) {
    this.date = date2;
  }
  public synchronized Integer getClientID() {
    return clientID;
  }
  public synchronized void setClientID(Integer clientID) {
    this.clientID = clientID;
  }
  public synchronized Integer getEmployeeID() {
    return employeeID;
  }
  public synchronized void setEmployeeID(Integer employeeID) {
    this.employeeID = employeeID;
  }
  public synchronized Integer getParkingPlaceID() {
    return parkingPlaceID;
  }
  public synchronized void setParkingPlaceID(Integer parkingPlaceID) {
    this.parkingPlaceID = parkingPlaceID;
  }
  public synchronized Integer getCarNumber() {
    return carNumber;
  }
  public synchronized void setCarNumber(Integer carNumber) {
    this.carNumber = carNumber;
  }
  
  
}
