package com.dao.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
@Table(name = "\"PlaceEvents\"")
public class PlaceEventDAO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5266794916299043598L;
  @Id
  @Column(name = "id")
  public Integer eventID;
  @OneToOne
  @OnDelete(action = OnDeleteAction.CASCADE)
  @JoinColumn(name = "place", referencedColumnName = "id")
  public ParkingPlaceDAO placeID;
  @Column(name = "date")
  public Date date;
  @OneToOne
  @JoinColumn(name = "event", referencedColumnName = "name")
  public EventDAO event;
  
  
  public synchronized Integer getEventID() {
    return eventID;
  }
  public synchronized void setEventID(Integer eventID) {
    this.eventID = eventID;
  }
  public synchronized ParkingPlaceDAO getPlaceID() {
    return placeID;
  }
  public synchronized void setPlaceID(ParkingPlaceDAO placeID) {
    this.placeID = placeID;
  }
  public synchronized Date getDate() {
    return date;
  }
  public synchronized void setDate(Date date) {
    this.date = date;
  }
  public synchronized EventDAO getEvent() {
    return event;
  }
  public synchronized void setEvent(EventDAO event) {
    this.event = event;
  }





}
