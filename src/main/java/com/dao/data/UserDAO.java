package com.dao.data;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import javax.persistence.JoinColumn;

@Entity
@Table(name = "\"Users\"")

public class UserDAO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1754748989816982163L;
  @Column(name = "name")
  public String name;
  @Id
  @Column(name = "login")
  public String login;
  @Column(name = "password")
  public String password;
  @Column(name = "birthdate")
  public Date birthdate;
  @Column(name = "email")
  public String email;
  
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "\"User_Roles\"", 
          joinColumns = {@JoinColumn(name = "login", nullable = false, updatable = false) }, 
          inverseJoinColumns = { @JoinColumn(name = "id", nullable = false, updatable = false) })
  private Set<RolesDAO> roles = new HashSet<RolesDAO>(0);

  public UserDAO(String name, String login, String password, Date birthdate, String email,
      Set<RolesDAO> roles) {
    super();
    this.name = name;
    this.login = login;
    this.password = password;
    this.birthdate = birthdate;
    this.email = email;
    this.roles = roles;
  }

  public UserDAO(String name, String login, String password) {
    super();
    this.name = name;
    this.login = login;
    this.password = password;
  }

  public UserDAO() {
    super();
  }

  public synchronized String getName() {
    return name;
  }

  public synchronized void setName(String name) {
    this.name = name;
  }

  public synchronized String getLogin() {
    return login;
  }

  public synchronized void setLogin(String login) {
    this.login = login;
  }

  public synchronized String getPassword() {
    return password;
  }

  public synchronized void setPassword(String password) {
    this.password = password;
  }

  public synchronized Date getBirthdate() {
    return birthdate;
  }

  public synchronized void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }

  public synchronized String getEmail() {
    return email;
  }

  public synchronized void setEmail(String email) {
    this.email = email;
  }

  public synchronized Set<RolesDAO> getRoles() {
    return roles;
  }

  public synchronized void setRoles(Set<RolesDAO> roles) {
    this.roles = roles;
  }
  
  
  
  
}