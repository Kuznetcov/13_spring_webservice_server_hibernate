package com.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.dao.data.DealDAO;
import com.dao.data.EventDAO;
import com.dao.data.ParkingPlaceDAO;
import com.dao.data.PeopleDAO;
import com.dao.data.PlaceEventDAO;
import com.dao.data.UserDAO;
import com.data.Client;
import com.data.Deal;
import com.data.Event;
import com.data.ParkingPlace;
import com.data.User;

@Transactional
public class UseHibernate implements DataBaseDAO {

  @Autowired
  private HibernateTemplate hibernateTemplate;

  static Logger log = LoggerFactory.getLogger(UseHibernate.class);
  
  @Override
  public Client getClient(Integer id) {
    Client client = new Client();
    PeopleDAO cdao = hibernateTemplate.load(PeopleDAO.class, id);
    client.setId(cdao.getId());
    client.setName(cdao.getName());
    return client;
  }

  @Override
  public String addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber) {

    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    Date date2;
    try {
      date2 = fmt.parse(date);

      DealDAO deal = new DealDAO();
      deal.setDealID(dealID);
      deal.setDate(date2);
      deal.setClientID(clientID);
      deal.setEmployeeID(employeeID);
      deal.setParkingPlaceID(parkingPlaceID);
      deal.setCarNumber(carNumber);
      hibernateTemplate.saveOrUpdate(deal);
      return new String("Сделка " + dealID + " успешно сохранена в базе");
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return "ParseException";
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Deal> getDealsByClientID(Integer clientID) {

    DetachedCriteria dcrit = DetachedCriteria.forClass(DealDAO.class);
    dcrit.add(Restrictions.eq("clientID", clientID));
    List<DealDAO> deals = (List<DealDAO>) hibernateTemplate.findByCriteria(dcrit);
    List<Deal> result = new ArrayList<>();
    deals.stream().forEach((d) -> {
      result.add(new Deal(d.getDealID(), d.getDate().toString(), d.getClientID(), d.getEmployeeID(),
          d.getParkingPlaceID(), d.getCarNumber()));
    });
    return result;
  }

  @Override
  public String addPlaceEvent(Integer eventID, Integer placeID, String date, Event event) {

    DetachedCriteria dcrit = DetachedCriteria.forClass(EventDAO.class);
    dcrit.add(Restrictions.eq("name", event.getName()));
    EventDAO eventType = (EventDAO) hibernateTemplate.findByCriteria(dcrit).get(0);


    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    Date date2;
    try {
      date2 = fmt.parse(date);
      PlaceEventDAO placeEvent = new PlaceEventDAO();
      placeEvent.setEventID(eventID);
      placeEvent.setPlaceID(hibernateTemplate.get(ParkingPlaceDAO.class, placeID));
      placeEvent.setDate(date2);
      placeEvent.setEvent(eventType);
      hibernateTemplate.saveOrUpdate(placeEvent);
      return new String("Event " + eventID + " successfully saved in database");
    } catch (ParseException e) {
      e.printStackTrace();
      return "ParseException";
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Deal> getDeal(Deal deal) {

    List<DealDAO> deals = (List<DealDAO>) hibernateTemplate
        .find("FROM DealDAO d WHERE d.clientID=?", deal.getClientID());
    List<Deal> result = new ArrayList<>();
    deals.stream().forEach((d) -> {
      result.add(new Deal(d.getDealID(), d.getDate().toString(), d.getClientID(), d.getEmployeeID(),
          d.getParkingPlaceID(), d.getCarNumber()));
    });
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public String updateDealEmployee(Integer employeeIDFrom, Integer employeeIDTo) {

    System.out.println(hibernateTemplate.getSessionFactory().getCurrentSession().hashCode());

    DetachedCriteria dcrit = DetachedCriteria.forClass(DealDAO.class);
    dcrit.add(Restrictions.eq("employeeID", employeeIDFrom));
    List<DealDAO> deals = (List<DealDAO>) hibernateTemplate.findByCriteria(dcrit);

    deals.stream().forEach((d) -> {
      d.setEmployeeID(employeeIDTo);
      hibernateTemplate.update(d);
      try {
        Thread.sleep(1000);
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    });

    return "Updated";
  }

  @Transactional
  @Override
  public String addUser(String name, String login, String password) {
    UserDAO user = new UserDAO();
    user.setName(name);
    user.setLogin(login);
    user.setPassword(password);
    hibernateTemplate.saveOrUpdate(user);
    return new String("Сделка " + user + " успешно сохранена в базе");
  }

  @SuppressWarnings("unchecked")
  @Override
  public String getUser(String login, String password) {
    String resultName = null;
    DetachedCriteria dcrit = DetachedCriteria.forClass(UserDAO.class);
    dcrit.add(Restrictions.eq("login", login));
    dcrit.add(Restrictions.eq("password", password));
    List<UserDAO> users = (List<UserDAO>) hibernateTemplate.findByCriteria(dcrit);
    List<User> result = new ArrayList<>();
    users.stream().forEach((d) -> {
      result.add(new User(d.getName(), d.getLogin().toString(), d.getPassword()));
    });
    if (result.size() > 0)
      resultName = result.get(0).name;

    return resultName;
  }
  
  

  @SuppressWarnings("unchecked")
  @Override
  public List<ParkingPlace> getParkingPlaces() {

    List<ParkingPlaceDAO> places =
        (List<ParkingPlaceDAO>) hibernateTemplate.find("FROM ParkingPlaceDAO d");
    List<ParkingPlace> result = new ArrayList<>();
    places.stream().forEach((d) -> {
      result.add(new ParkingPlace(d.getPlaceID(), d.getAvaliable(), d.getOwner()));
    });
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ParkingPlace> getParkingPlacesWithPaging(int from, int max) {


    List<ParkingPlaceDAO> places = (List<ParkingPlaceDAO>) hibernateTemplate.execute(
        new HibernateCallback<Object>() {
          public Object doInHibernate(org.hibernate.Session session) throws HibernateException {
            Query query = session.createQuery("FROM ParkingPlaceDAO d");
            query.setFirstResult(from);
            query.setMaxResults(max);
            return query.list();
          }
        });
    

    List<ParkingPlace> result = new ArrayList<>();
    places.stream().forEach((d) -> {
      result.add(new ParkingPlace(d.getPlaceID(), d.getAvaliable(), d.getOwner()));
    });

    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public User getUserByLogin(String login) {
    User resultUser = null;
    DetachedCriteria dcrit = DetachedCriteria.forClass(UserDAO.class);
    dcrit.add(Restrictions.eq("login", login));
    List<UserDAO> users = (List<UserDAO>) hibernateTemplate.findByCriteria(dcrit);
    List<User> result = new ArrayList<>();
    users.stream().forEach((d) -> {
      result.add(new User(d.getName(),d.getLogin(),d.getPassword(),d.getBirthdate(),d.getEmail(),d.getRoles()));
    });
    if (result.size() > 0)
      resultUser = result.get(0);
    return resultUser;
  }

  @Override
  public String addUserWithRoles(User user) {
    UserDAO userToDB = new UserDAO();
    userToDB.setName(user.getName());
    userToDB.setLogin(user.getLogin());
    userToDB.setPassword(user.getPassword());
    userToDB.setBirthdate(user.getBirthDate());
    userToDB.setEmail(user.getEmail());
    userToDB.setRoles(user.getRoles());
    hibernateTemplate.merge(userToDB);
    return new String(user.getName());
  }

  @Override
  public String addParkingPlace(ParkingPlace place) {
    ParkingPlaceDAO placeToDB = new ParkingPlaceDAO();
    placeToDB.setPlaceID(place.getPlaceID());
    placeToDB.setAvaliable(place.getAvaliable());
    placeToDB.setOwner(place.getOwner());
    hibernateTemplate.saveOrUpdate(placeToDB);
    return new String(place.getPlaceID().toString());
  }

  @Override
  public String deleteParkingPlace(Integer placeID) {
    try{
    hibernateTemplate.delete(hibernateTemplate.get(ParkingPlaceDAO.class, placeID));
    }
    catch(DataAccessException ex){
      log.info(ex.getMessage());
    }
    return placeID.toString();
  }
}
