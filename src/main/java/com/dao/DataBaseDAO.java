package com.dao;


import java.util.List;

import com.data.Client;
import com.data.Deal;
import com.data.Event;
import com.data.ParkingPlace;
import com.data.User;

public interface DataBaseDAO {

  public Client getClient(Integer id);
  
  public String addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber);
  
  public List<Deal> getDealsByClientID(Integer clientID);
  
  public String addPlaceEvent(Integer eventID, Integer placeID, String date, Event event);
  
  public List<Deal> getDeal(Deal deal);
  
  public String updateDealEmployee(Integer employeeIDFrom, Integer employeeIDTo);

  public String addUser(String name, String login, String password);
  
  public String addUserWithRoles(User user);

  public String getUser(String login, String password);

  public List<ParkingPlace> getParkingPlaces();
  
  public List<ParkingPlace> getParkingPlacesWithPaging(int from, int max);
  
  public User getUserByLogin(String login);
  
  public String addParkingPlace(ParkingPlace place);
  
  public String deleteParkingPlace(Integer placeID);
  
}
