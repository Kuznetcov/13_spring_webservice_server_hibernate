package com.example;

import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.dao.DataBaseDAO;
import com.dao.UseHibernate;

@Configuration
public class DBConfig {

  @Resource
  private Environment env;

  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getRequiredProperty("DriverClassName"));
    dataSource.setUrl(env.getRequiredProperty("URL"));
    dataSource.setUsername(env.getProperty("name"));
    dataSource.setPassword(env.getRequiredProperty("Password"));
    return dataSource;
  }

  @Bean
  public DataBaseDAO dataBaseDAO() {
    return new UseHibernate();
  }


  @Bean
  @Autowired
  public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
    HibernateTransactionManager htm = new HibernateTransactionManager();
    htm.setSessionFactory(sessionFactory);
    return htm;
  }

  @Bean
  @Autowired
  public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory) {
    HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
    return hibernateTemplate;
  }

  @Bean
  public Properties getHibernateProperties() {
    Properties properties = new Properties();
    properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
    properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
    properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
    properties.put("hibernate.temp.use_jdbc_metadata_defaults", env.getProperty("hibernate.temp.use_jdbc_metadata_defaults"));
    
    return properties;
  }

  @Bean
  public LocalSessionFactoryBean getSessionFactory() {
    LocalSessionFactoryBean asfb = new LocalSessionFactoryBean();
    asfb.setDataSource(dataSource());
    asfb.setHibernateProperties(getHibernateProperties());
    asfb.setPackagesToScan(new String[] {"com"});
    return asfb;
  }



  @Bean
  public TaskExecutor taskExecutor(){
    return new ThreadPoolTaskExecutor();
  }

}
