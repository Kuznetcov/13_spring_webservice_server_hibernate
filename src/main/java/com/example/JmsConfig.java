package com.example;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;

@Configuration
public class JmsConfig {

  @Bean
  public ActiveMQConnectionFactory amqConnectionFactory(){
    
    return new ActiveMQConnectionFactory("failover:tcp://localhost:61616");
  }
  
  @Bean
  @Autowired
  public ConnectionFactory connectionFactory(ActiveMQConnectionFactory amqConnectionFactory){
    return new CachingConnectionFactory(amqConnectionFactory);
  }
  
  @Bean
  public ActiveMQQueue defaultDestination(){
    return new ActiveMQQueue("Send2Recv");
  }
  
  @Bean // Strictly speaking this bean is not necessary as boot creates a default  - контейнер Listeners, можно настроить количество потоков
  JmsListenerContainerFactory<?> myJmsContainerFactory(ConnectionFactory connectionFactory) {
      SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
      factory.setConnectionFactory(connectionFactory);
      return factory;
  }


}
