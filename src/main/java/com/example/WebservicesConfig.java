package com.example;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.webservice.rest.RestServiceInterface;
import com.webservice.soap.SoapTest;
import com.webservice.soap.SoapTestImpl;

@Configuration
@ComponentScan("com.webservice")
public class WebservicesConfig {

  @Autowired
  SoapTest soapTest;

  @Autowired
  RestServiceInterface rest;

  @Bean
  public ServletRegistrationBean dispatcherServlet() {

    CXFServlet servlet = new CXFServlet();
    return new ServletRegistrationBean(servlet, "/soap-api/*");
  }

  @Bean(name = Bus.DEFAULT_BUS_ID)
  public SpringBus springBus() {
    return new SpringBus();
  }

  @Bean
  public SoapTest soapTest() {
    return new SoapTestImpl();
  }

  @Bean
  public Server jaxRsServer() {
    JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
    factory.setBus(springBus());
    JacksonJaxbJsonProvider jp = new JacksonJaxbJsonProvider();    
    factory.setProvider(jp);
    factory.setServiceBean(rest);
    factory.setAddress("/rest");
    return factory.create();
  }

  @Bean
  public Endpoint endpoint() {
    EndpointImpl endpoint = new EndpointImpl(springBus(), soapTest);
    endpoint.publish("/getusername");
    return endpoint;
  }


}
