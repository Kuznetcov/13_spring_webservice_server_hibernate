package com.webservice.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.DataBaseDAO;
import com.dao.data.RolesDAO;
import com.data.ParkingPlace;
import com.data.PlaceEvent;
import com.data.User;
import com.mongodb.ParkingPlaceRepository;
import com.mongodb.dao.CommentMongo;
import com.mongodb.dao.ParkingPlaceMongo;

@Service("restService")
public class RestServiceImpl implements RestServiceInterface {

  @Autowired
  private DataBaseDAO dataBaseDAO;

  @Autowired
  private ParkingPlaceRepository mongoRepository;

  @Override
  public User getUser(String login, String password) {
    User tmp = new User(dataBaseDAO.getUser(login, password), login, password);
    return tmp;
  }

  @Override
  public List<ParkingPlace> getPlaces() {
    List<ParkingPlace> list = dataBaseDAO.getParkingPlaces();
    return list;
  }

  @Override
  public Response newUser(User user) {

    String result = "User added : " + user;
    dataBaseDAO.addUser(user.getName(), user.getLogin(), user.getPassword());
    return Response.status(201).entity(result).build();
  }

  @Override
  public Response addPlaceEvent(PlaceEvent event) {
    String result = "Event added : " + event;
    dataBaseDAO.addPlaceEvent(event.getEventID(), event.getPlaceID(), event.getDate(),
        event.getEvent());
    return Response.status(201).entity(result).build();
  }

  @Override
  public List<ParkingPlace> getPlacesWithPaging(String from, String max) {
    List<ParkingPlace> list =
        dataBaseDAO.getParkingPlacesWithPaging(Integer.valueOf(from), Integer.valueOf(max));
    return list;
  }

  @Override
  public User getUser(String login) {
    return dataBaseDAO.getUserByLogin(login);
  }

  @Override
  public Response newUserWithRole(User user) {
    String result = "User added : " + user;
    System.out.println(user.toString());
    RolesDAO roles = new RolesDAO(1, "REGISTERED_USER");
    user.roles.add(roles);
    System.out.println(user.toString());
    dataBaseDAO.addUserWithRoles(user);
    return Response.status(201).entity(result).build();
  }

  @Override
  public Response newParkingPlace(ParkingPlace place) {
    String result = "ParkingPlace added : " + place;
    dataBaseDAO.addParkingPlace(place);
    return Response.status(201).entity(result).build();
  }

  @Override
  public Response deleteParkingPlace(String placeID) {
    String result = "ParkingPlace deleted : " + placeID;
    try {
      dataBaseDAO.deleteParkingPlace(Integer.valueOf(placeID));
      return Response.status(201).entity(result).build();
    } catch (NumberFormatException ex) {
      result = ex.getMessage();
    }
    return Response.status(204).entity(result).build();
  }



  @Override
  public Response addComment(String placeID, CommentMongo comment) {
    ParkingPlaceMongo document = mongoRepository.findByPlaceID(Integer.valueOf(placeID));
    if (document != null) {
      document.addComment(comment);
      mongoRepository.save(document);
      return Response.status(200).entity(placeID).build();
    } else {
      document = new ParkingPlaceMongo();
      document.setPlaceID(Integer.valueOf(placeID));
      document.addComment(comment);
      mongoRepository.save(document);
      return Response.status(201).entity(placeID).build();
    }
  }

  @Override
  public ParkingPlaceMongo getPlaceInfo(String placeID) {
    ParkingPlaceMongo document = mongoRepository.findByPlaceID(Integer.valueOf(placeID));
    return document;
  }

  @Override
  public List<CommentMongo> getCommentsByText(String text) {
    List<ParkingPlaceMongo> places = mongoRepository.findByCommentsTextLike(text);
    List<CommentMongo> result = new ArrayList<>();
    result = places.stream().map(ParkingPlaceMongo::getComments)
                            .flatMap(x -> x.stream())
                            .filter(c -> c.getText().contains(text))
                            .collect(Collectors.toList());
    return result;
  }
}
