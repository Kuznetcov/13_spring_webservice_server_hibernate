package com.webservice.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.data.ParkingPlace;
import com.data.PlaceEvent;
import com.data.User;
import com.mongodb.dao.CommentMongo;
import com.mongodb.dao.ParkingPlaceMongo;

@Path("/service")
@Produces("application/json")
public interface RestServiceInterface {

  @GET
  @Path("/{login}/{password}")
  public User getUser(@PathParam("login") String login, @PathParam("password") String password);
  
  @GET
  @Path("/{login}")
  public User getUser(@PathParam("login") String login);

  @GET
  @Path("/places")
  public List<ParkingPlace> getPlaces();
  
  @PUT
  @Path("/newuser")
  @Consumes("application/json")
  public Response newUser(User user);
  
  @PUT
  @Path("/placeevent")
  @Consumes("application/json")
  public Response addPlaceEvent(PlaceEvent event);
  
  @GET
  @Path("/placeswithpaging")
  public List<ParkingPlace> getPlacesWithPaging(
      @DefaultValue("0")@QueryParam("from") String from,
      @DefaultValue("9000")@QueryParam("max") String max);
  
  @PUT
  @Path("/newuserwithrole")
  @Consumes("application/json")
  public Response newUserWithRole(User user);
  
  @PUT
  @Path("/parkingplace/add")
  @Consumes("application/json")
  public Response newParkingPlace(ParkingPlace place);
  
  @DELETE
  @Path("/parkingplace/delete/{placeid}")
  public Response deleteParkingPlace(@PathParam("placeid") String placeID);
  
  @PUT
  @Path("/parkingplace/{placeID}/newcomment")
  public Response addComment(@PathParam("placeID") String placeID,CommentMongo comment);
  
  @GET
  @Path("/parkingplace/{placeID}")
  public ParkingPlaceMongo getPlaceInfo(@PathParam("placeID") String placeID);
  
  @GET
  @Path("/find/{text}")
  public List<CommentMongo> getCommentsByText(@PathParam("text") String text);
  
}
