package com.webservice.soap;
 

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import com.data.Client;
import com.data.Deal;
import com.data.Event;

@WebService
public interface SoapTest {

  @WebMethod
  public String sayHello();


  @WebMethod
  public Client getClient(@XmlElement(required=true)@WebParam(name="ClientID")Integer id);


  @WebMethod
  public String addDeal(
      @XmlElement(required=true)
      @WebParam(name="dealID") 
      Integer dealID, 
      
      @XmlElement(required=true)
      @WebParam(name="date") 
      String date, 
      
      @XmlElement(required=true)
      @WebParam(name="clientID") 
      Integer clientID, 
      
      @XmlElement(required=true)
      @WebParam(name="employeeID") 
      Integer employeeID, 
      
      @XmlElement(required=true)
      @WebParam(name="parkingPlaceID") 
      Integer parkingPlaceID, 
      
      @XmlElement(required=true)
      @WebParam(name="carNumber") 
      Integer carNumber
      );
  
  
  @WebMethod
  public List<Deal> getDealsByClientID(@XmlElement(required=true)@WebParam(name="ClientID")Integer clientID);
  
  
  @WebMethod
  public List<Deal> getDeal(@XmlElement(required=true)@WebParam(name="Deal")Deal deal);
  
  
  @WebMethod
  public String addPlaceEvent(
      @XmlElement(required=true)
      @WebParam(name="EventID") 
      Integer eventID,
      
      @XmlElement(required=true)
      @WebParam(name="PlaceID") 
      Integer placeID, 
      
      @XmlElement(required=true)
      @WebParam(name="Date") 
      String date,
      
      @XmlElement(required=true)
      @WebParam(name="Event") 
      Event event
      );
  
  
}
