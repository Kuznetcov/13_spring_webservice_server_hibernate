package com.webservice.soap;
import java.util.List;
import javax.jws.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import com.dao.DataBaseDAO;
import com.data.Client;
import com.data.Deal;
import com.data.Event;

@WebService(endpointInterface = "com.webservice.soap.SoapTest")
public class SoapTestImpl implements SoapTest {

  @Autowired
  private DataBaseDAO dataBaseDAO;


  @Override
  public String sayHello() {
    return "Hello";
  }

  @Override
  public Client getClient(Integer id) {
    return dataBaseDAO.getClient(id);
  }

  @Override
  public String addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber) {
    return dataBaseDAO.addDeal(dealID, date, clientID, employeeID, parkingPlaceID, carNumber);
  }

  @Override
  public List<Deal> getDealsByClientID(Integer clientID) {
    return dataBaseDAO.getDealsByClientID(clientID);
  }

  @Override
  public String addPlaceEvent(Integer eventID, Integer placeID, String date, Event event) {
    return dataBaseDAO.addPlaceEvent(eventID, placeID, date, event);
  }

  @Override
  public List<Deal> getDeal(Deal deal) {
    return dataBaseDAO.getDeal(deal);
  }

}
