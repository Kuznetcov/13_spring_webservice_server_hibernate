package com.jms;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;

import com.data.ParkingPlace;
import com.data.PlaceEvent;
import com.data.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webservice.rest.RestServiceInterface;

@Component
public class Receiver {

  @Autowired
  RestServiceInterface database;
  
  ObjectMapper mapper = new ObjectMapper();
  
  @JmsListener(destination = "Send2Recv", containerFactory = "myJmsContainerFactory")
  public String receiveMessage(String message) {
    System.out.println("Received <" + message + ">");
    FileSystemUtils.deleteRecursively(new File("activemq-data"));
    return new String("Received <" + message + ">");
  }
  
  @JmsListener(destination = "GetUser", containerFactory = "myJmsContainerFactory")
  public String getUser(String message) {
    User user = database.getUser(message);
    String result="";
    try {
      result = mapper.writeValueAsString(user);
    } catch (JsonProcessingException e) {
      result = e.getMessage();
      e.printStackTrace();
    }
    return result;
  }
  
  @JmsListener(destination = "GetPlaces", containerFactory = "myJmsContainerFactory")
  public String getPlaces(String message) {
    List<ParkingPlace> places = database.getPlaces();
    String result="";
    try {
      result = mapper.writeValueAsString(places);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      result = e.getMessage();
    }
    return result;
  }
  
  @JmsListener(destination = "AddUser", containerFactory = "myJmsContainerFactory")
  public String newUser(String message) {
    User user = null;
    
    try {
      user = mapper.readValue(message, User.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    Response result = database.newUserWithRole(user);
    return Integer.toString(result.getStatus());
  }

  @JmsListener(destination = "AddPlaceEvent", containerFactory = "myJmsContainerFactory")
  public String addPlaceEvent(String message) {
    PlaceEvent event = null;
    try {
      event = mapper.readValue(message, PlaceEvent.class);
      Response result = database.addPlaceEvent(event);
      return Integer.toString(result.getStatus());
    } catch (IOException e) {
      e.printStackTrace();
      return e.getMessage();
    } 
  }

  @JmsListener(destination = "DeleteParkingPlace", containerFactory = "myJmsContainerFactory")
  public String deleteParkingPlace(String message) {
    Response result = database.deleteParkingPlace(message);
    return Integer.toString(result.getStatus());
  }

}
