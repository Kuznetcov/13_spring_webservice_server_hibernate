package com.mongodb.dao;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;

public class CommentMongo {

  @Id
  private Integer id;
  private String username;
  @TextIndexed
  private String text;
  private Integer rating;

  public CommentMongo(Integer id, String username, String text, Integer rating) {
    super();
    this.id = id;
    this.username = username;
    this.text = text;
    this.rating = rating;
  }

  public CommentMongo() {
    super();
  }

  public synchronized Integer getId() {
    return id;
  }

  public synchronized void setId(Integer id) {
    this.id = id;
  }

  public synchronized String getUsername() {
    return username;
  }

  public synchronized void setUsername(String username) {
    this.username = username;
  }

  public synchronized String getText() {
    return text;
  }

  public synchronized void setText(String text) {
    this.text = text;
  }

  public synchronized Integer getRating() {
    return rating;
  }

  public synchronized void setRating(Integer rating) {
    this.rating = rating;
  }

  @Override
  public String toString() {
    return "CommentMongo [id=" + id + ", username=" + username + ", text=" + text + ", rating="
        + rating + "]";
  }

}
