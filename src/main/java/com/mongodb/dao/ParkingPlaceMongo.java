package com.mongodb.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ParkingPlaceMongo {


  @Id
  private Integer placeID;
  private List<CommentMongo> comments = new ArrayList<>();
  private String ind;

  public ParkingPlaceMongo(Integer placeID, List<CommentMongo> comments, String ind) {
    super();
    this.placeID = placeID;
    this.comments = comments;
    this.ind = ind;
  }

  public synchronized String getInd() {
    return ind;
  }

  public synchronized void setInd(String ind) {
    this.ind = ind;
  }

  private Integer rating;

  public ParkingPlaceMongo(Integer placeID, List<CommentMongo> comments) {
    super();
    this.placeID = placeID;
    this.comments = comments;
  }

  public ParkingPlaceMongo() {
    super();
  }

  public synchronized Integer getPlaceID() {
    return placeID;
  }

  public synchronized void setPlaceID(Integer placeID) {
    this.placeID = placeID;
  }

  public synchronized Integer getRating() {
    return rating;
  }

  public synchronized List<CommentMongo> getComments() {
    return comments;
  }

  public synchronized void setComments(List<CommentMongo> comments) {
    this.comments = comments;
  }

  public void addComment(CommentMongo comment) {
    if (this.comments.size() != 0) {
      CommentMongo lastComment =
      this.comments.stream().max((c1, c2) -> Integer.compare(c1.getId(), c2.getId())).get();
      comment.setId(lastComment.getId() + 1);
    } else {
      comment.setId(1);
    }
    this.comments.add(comment);
  }

  @Override
  public String toString() {
    return "ParkingPlaceMongo [placeID=" + placeID + ", comments=" + comments + "]";
  }

}
