package com.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.dao.ParkingPlaceMongo;

@Repository
public interface ParkingPlaceRepository extends MongoRepository<ParkingPlaceMongo, Integer> {

  public ParkingPlaceMongo findByPlaceID(Integer id);  //ИМПЛЕМЕНТАЦИИ НЕТ!!!! СРСЛИ! НЕТ! РАБОТАЕТ!
  
  @Query(value="{}", fields="{comments: {$elemMatch: {id:?0}}}")
  public ParkingPlaceMongo getByCommentsId(Integer id);   
  
  @Query(value="{}",fields="{comments: {$elemMatch: {text: {$regex: ?0}}}}")
  public ParkingPlaceMongo getByCommentsText(String t);
  
  @Query(value="{$text:{$search:?0}}")
  public List<ParkingPlaceMongo> getByCommentsTextIndex(String t);
  
  //@Query(value="{$text: {$search: '?0'}}")
  //@Query(fields="{placeID:1}")
  public List<ParkingPlaceMongo> findByCommentsTextLike(String t);
  
}