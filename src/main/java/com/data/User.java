package com.data;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import com.dao.data.RolesDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

  public String name;
  public String login;
  public String password;
  public Date birthDate;
  public String email;
  public Set<RolesDAO> roles = new HashSet<RolesDAO>(0);
  public User(String name, String login, String password, Date birthDate, String email,
      Set<RolesDAO> roles) {
    super();
    this.name = name;
    this.login = login;
    this.password = password;
    this.birthDate = birthDate;
    this.email = email;
    this.roles = roles;
  }
  public User(String name, String login, String password) {
    super();
    this.name = name;
    this.login = login;
    this.password = password;
  }
  public User() {
    super();
  }
  public synchronized String getName() {
    return name;
  }
  public synchronized void setName(String name) {
    this.name = name;
  }
  public synchronized String getLogin() {
    return login;
  }
  public synchronized void setLogin(String login) {
    this.login = login;
  }
  public synchronized String getPassword() {
    return password;
  }
  public synchronized void setPassword(String password) {
    this.password = password;
  }
  public synchronized Date getBirthDate() {
    return birthDate;
  }
  public synchronized void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }
  public synchronized String getEmail() {
    return email;
  }
  public synchronized void setEmail(String email) {
    this.email = email;
  }
  public synchronized Set<RolesDAO> getRoles() {
    return roles;
  }
  public synchronized void setRoles(Set<RolesDAO> roles) {
    this.roles = roles;
  }
  @Override
  public String toString() {
    return "User [name=" + name + ", login=" + login + ", password=" + password + ", birthDate="
        + birthDate + ", email=" + email + ", roles=" + roles + "]";
  }
  
}
